import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;

/**
 * Create a class that count how many times different words occur. Each time the addWord()
 * method is called, you add to the occurrences for that word. Ignore the case of words in
 * counting words. 
 * @author Pipatpol Tanavongchinda
 */
public class WordCounter {
	Map<String,Integer> allWords = new HashMap<String,Integer>();

	/**
	 * Add one the count of occurrences for this word, ignoring case.
	 * @param word is the word that contain in the text.
	 */
	public void addWord(String word)
	{
		if(allWords.containsKey(word))
		{
			Integer cursor = new Integer(allWords.get(word).intValue()+1);
			allWords.put(word, cursor++);
		}
		else
		{
			allWords.put(word, new Integer(1));
		}
	}
	/**
	 * Get all the words seen so far, as a Set.
	 * @return All of the words has seen.
	 */
	public Set<String> getWords()
	{
		return allWords.keySet();
	}
	/**
	 * Get the number of occurrences for a given word.
	 * @param word is the word that you want to know frequency in this text.
	 * @return Amount of this word.
	 */
	public int getCount(String word)
	{
		return allWords.get(word).intValue();
	}
	/**
	 * Get all the words seen so far, sorted in alphabetical order.
	 * @return An array of all words.
	 */
	public String[] getSortedWords()
	{
		ArrayList<String> wordList = new ArrayList<String>();
		for (String string : allWords.keySet()) {
			wordList.add(string);
		}
		Collections.sort(wordList);
		String[] sortedWord = wordList.toArray(new String[0]);
		return sortedWord;
	}
	/**
	 * Clone an array of getSortedWords.
	 * @param size is size of getSortedWords.
	 * @param array is an array of getSortedWords.
	 * @return cloning of getSortedWords.
	 */
	public static String[] clone(int size,String[] array){
		String[] arrImport = array;
		String[] clone = new String[size];
		for(int i = 0;i<clone.length;i++){
			clone[i] = arrImport[i];
		}
		return clone;
	}
	/**
	 * Get all the words seen so far, sorted from frequency descending.
	 * Using Selection sort method.
	 * @return An array of all word in frequency descending.
	 */
	public String[] getSortedByValue()
	{
		String[] pleaseSortMe = getSortedWords();
		String[] cloning = clone(pleaseSortMe.length,pleaseSortMe);
		
		for(int i = 0;i<pleaseSortMe.length;i++){
			for(int j = 0;j<(pleaseSortMe.length-1)-i;j++){
				if(getCount(pleaseSortMe[i])<getCount(pleaseSortMe[j+i+1])){
					pleaseSortMe[i] = cloning[j+i+1];
					pleaseSortMe[j+i+1] = cloning[i];
					cloning = clone(pleaseSortMe.length,pleaseSortMe);
				}
			}
		}
		return cloning;
		
	}
}
